#!/usr/bin/env python3.5
import os
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext

CHECKPOINT_DIR = "/tmp/spark-checkpoint"

def updateFunc(newValues, runningCount):
    if runningCount is None:
        runningCount = 0
    return sum(newValues, runningCount)

def ShakespeareCount(streamingContext, textFilePath):
    wordStream = streamingContext.textFileStream("file://" + textFilePath) \
                                 .flatMap(lambda line: line.split("\n")) \
                                 .map(lambda line: line.strip()) \
                                 .filter(lambda line: line !=  "") \
                                 .flatMap(lambda line: line.split(" ")) \
                                 .map(lambda word : (word,1)) \
                                 .reduceByKey(lambda x, y: x + y)
    
    stateDstream = wordStream.updateStateByKey(updateFunc)
    stateDstream.pprint()

def recoveryOrCreateSsc():
    shakespeareTextFolder = os.path.join(os.getcwd(),"filestream")
    conf = SparkConf().setAppName("shakespeare")
    sc = SparkContext(conf=conf) 
    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, 10)
    # Fissiamo la cartella di checkpoint
    ssc.checkpoint("file://" + CHECKPOINT_DIR)
    ShakespeareCount(ssc, shakespeareTextFolder) 
    
    return ssc
    

if __name__ == '__main__':

    if not os.path.exists(CHECKPOINT_DIR):
        os.makedirs(CHECKPOINT_DIR)
              
    ssc = StreamingContext.getOrCreate("file://" + CHECKPOINT_DIR, recoveryOrCreateSsc)
    ssc.sparkContext.setLogLevel("WARN")
    ssc.start()
    ssc.awaitTermination()
