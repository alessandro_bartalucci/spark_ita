#!/usr/bin/env python3.5
import random
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

batchInterval = 1                # dimensione del batch
windowLength = 50 * batchInterval # lunghezza della finestra
frequency = 1 * batchInterval    # frequenza di slide


if __name__ == '__main__':
    # definiamo il contesto sparkStreaming
    sc = SparkContext("local[2]", "window")
    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, batchInterval)
    # creiamo un array vuoto
    rddArray = []
    # creiamo una sequenza di rdd ciascuno contenente  un intero
    for n in range(1,1001):
        rddArray.append(sc.parallelize([random.gauss(0,1)]))
    # immettiamo questi rdd in una coda stream uno alla volta
    raw = ssc.queueStream(rddArray, oneAtATime=True).map(lambda x : float(x))
    # creiamo un dstream col valore attuale immesso nella coda
    value = raw.map(lambda x : {"value": x})
    # creiamo un dstream windowed in cui sommiamo i valori nella finestra 
    lastTwoSum = raw \
             .window(windowDuration=windowLength, slideDuration=frequency) \
             .transform(lambda x : ssc.sparkContext.parallelize([x.sum()/windowLength])) \
             .map(lambda x : {"lastTwoSum": x})
    # uniamo i due dstream e stampiamo a video
    output = ssc.union(value, lastTwoSum).pprint()
    # avviamo lo stream
    ssc.start()
    ssc.awaitTermination()
