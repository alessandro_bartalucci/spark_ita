#!/usr/bin/env python3.5

# spark-submit wordcount.py 9999
import sys

# la porta si può passare come argomento posizionale
if len(sys.argv) == 2:
   port = int(sys.argv[1])

# Importiamo le librerie SparkContext e StreamingContext
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext

conf = SparkConf().setAppName("wordcount")
sc = SparkContext(conf=conf)               # per far girare SparkStreaming abbiamo bisogno
ssc = StreamingContext(sc, 10)             # almeno 2 cores. Il range temporale è di 10 secondi

def WordCounter(streamingContext, port, host='localhost'):
    # Contiamo le parole che vengono ricevute da una SocketTextStream
    # e stampiamo a video le tuple all'interno del range di 10 secondi
    wordCount = ssc.socketTextStream(host, port) \
                   .flatMap(lambda text : text.split(" ")) \
                   .map(lambda word : (word, 1)) \
                   .reduceByKey(lambda x, y : x + y)
    wordCount.pprint(10000)
    ssc.start()
    ssc.awaitTermination()

if __name__ == '__main__':
    WordCounter(ssc, port)
