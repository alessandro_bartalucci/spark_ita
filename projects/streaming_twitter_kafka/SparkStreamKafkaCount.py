#!/usr/bin/env python3.5
import sys
import json

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

ZKQUORUM = "localhost:2181/kafka"
TOPIC = "twitter"

if __name__ == "__main__":

    # lunghezza del batch
    if len(sys.argv) != 2:
        print("Usage: %s <seconds_to_run>" % sys.argv[0])
        sys.exit(1)

    seconds_to_run = int(sys.argv[1])

    # Definiamo lo spark context
    sc = SparkContext("local[2]", appName="TwitterStreamKafka")
    sc.setLogLevel("ERROR")
    ssc = StreamingContext(sc, seconds_to_run)
    # Definiamo il Kafka Stream
    tweets = KafkaUtils.createStream(ssc, ZKQUORUM, "spark-streaming-consumer", {TOPIC: 1})
    # Ritorna dal json body del tweet la conta degli autori per ogni batch
    tweets.map(lambda body : json.loads(body[1])) \
          .map(lambda tweet: tweet['user']['screen_name']) \
          .countByValue() \
          .pprint()

    ssc.start()
    ssc.awaitTermination()
