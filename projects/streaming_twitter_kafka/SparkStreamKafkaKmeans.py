#!/usr/bin/env python3.5
import sys
import json
import os
import re
import numpy as np

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from pyspark.mllib.clustering import StreamingKMeans, KMeansModel
from pyspark.mllib.feature import Word2Vec

ZKQUORUM = "localhost:2181/kafka"
TOPIC = "twitter"

def features(rdd):
   if len(rdd.take(1)) > 0:
       wordModel = w2v.fit(rdd).getVectors()
       items = dict(wordModel).items()
       for t in items:
           print('{0:<20}       Cluster number = {1}'.format(t[0], trainedModel.predict(list(t[1])))) 
   else:
       print(None)

if __name__ == "__main__":

    # lunghezza del batch
    if len(sys.argv) != 3:
        print("Usage: %s <seconds_to_run> <modelDirectory> " % sys.argv[0])
        sys.exit(1)

    seconds_to_run = int(sys.argv[1])
    modelDirectory = sys.argv[2]

    assert os.path.exists(modelDirectory)
    assert seconds_to_run > 0

    # Definiamo lo spark context
    print("Initializing Streaming Spark Context...")
    sc = SparkContext("local[2]", appName="TwitterStreamKafka")
    sc.setLogLevel("ERROR")
    ssc = StreamingContext(sc, seconds_to_run)

    # Features
    w2v = Word2Vec().setMinCount(1).setVectorSize(100).setSeed(42)

    # Definiamo il Kafka Stream
    print("Initializing Kafka stream...")
    testingStream = KafkaUtils.createStream(ssc, ZKQUORUM, "spark-streaming-consumer", {TOPIC: 1})

    # Definiamo il modello
    print("Initializing il KMeans model...")
    trainedModel = KMeansModel.load(sc, modelDirectory)

    testingStreamClean = testingStream \
                   .map(lambda tweet : json.loads(tweet[1])) \
                   .map(lambda tweet : tweet.get("text")) \
                   .map(lambda line: line.lower() ) \
                   .map(lambda line: re.sub(r'[^\w\s]','',line)) \
                   .map(lambda line: line.replace('\n',' ')) \
                   .map(lambda line: line.split(" ") ) \
                   .map(lambda wordArray: [w for w in wordArray if w != "" and w != "rt"])
 
    testingStreamClean.reduce(lambda x, y: x + y).foreachRDD(features)
    
    ssc.start()
    ssc.awaitTermination()
