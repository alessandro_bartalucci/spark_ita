#! /usr/bin/env python3.5

import json
import sys

from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
from kafka import SimpleProducer, KafkaClient
 
TWITTER_CONSUMER_KEY = "ufAn9zLeKFEyyzRH9EyFWCk0u"
TWITTER_CONSUMER_SECRET = "0XUXtMH1WiNymW1WoxE5xJha95A21KtFXgWpnK3kPmC2NiKj57"
TWITTER_ACCESS_TOKEN = "947485612633927680-PgO0sA25xEj2rGXj9WLZrGTllQSua38"
TWITTER_ACCESS_SECRET = "88N3mNwMby2cKFaURmuCB2SG7jXjXFsFMwmk6Y7hwieuT"

KAFKA_TOPIC = "twitter"


class StdOutListener(StreamListener):
    def on_data(self, data):
        producer.send_messages(KAFKA_TOPIC, data.encode('utf-8'))
        print(data)
        return True

    def on_error(self, status):
        print(status)
        return True

if __name__ == '__main__': 

    if len(sys.argv) == 1:
        print("At least one word to track is required, add positional parameters! Exit now.")
        sys.exit(1)

    words = sys.argv[1:]
    
    # define producer
    kafka = KafkaClient('localhost:9092')
    producer = SimpleProducer(kafka)

    # define twitter listener 
    listener = StdOutListener()

    auth = OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_SECRET)
 
    twitter_stream = Stream(auth, listener)
    print("Sending data to kafka topic %s, tracking words: %s" % (KAFKA_TOPIC, ",".join(words)))
    while True:
        try:
           twitter_stream.filter(track=words)
        except AttributeError:
           pass
