#! /usr/bin/env python3.5

import json
import sys
import os
import shutil

from pyspark import SparkContext, SQLContext
from pyspark.sql.functions import lit, count
from pyspark.sql.types import ArrayType, StringType
from pyspark.ml.feature import HashingTF, RegexTokenizer, NGram, Word2Vec
from pyspark.ml.clustering import KMeans


if __name__ == '__main__': 

    if len(sys.argv) != 5:
        print("Usage: %s  <path to tweets> <save model path> <cluster number> <number of terations>" % (sys.argv[0]))
        sys.exit(1)

    tweets_path = sys.argv[1]
    save_model_path = sys.argv[2]
    numClusters = int(sys.argv[3])
    numIterations = int(sys.argv[4])
    
    assert numClusters > 0
    assert numIterations > 0

    if not os.path.exists(tweets_path):
        print("Non esiste il tweet path")
        sys.exit(1)
    if not os.path.exists(save_model_path):
        print("Non esiste il save model path")
        sys.exit(1)
    
    sc = SparkContext("local[*]", "tweetsToKmeans")
    sc.setLogLevel("WARN")
    sqlContext = SQLContext(sc)
    
    tweets = sc.textFile(tweets_path)
    print("Sample Tweet \n %s" % (tweets.take(1)))

    tweetsDF = sqlContext.read.json(tweets_path).cache()
    tweetsDF.registerTempTable("tweetTable")
    
    print("------Tweet table Schema---")
    tweetsDF.printSchema()

    print("----Sample Tweet Text-----")
    [ print(x) for x in sqlContext.sql("SELECT text FROM tweetTable LIMIT 10").collect() ]

    print("------Sample Lang, Name, text---")
    [ print(x) for x in sqlContext.sql("SELECT user.lang, user.name, text FROM tweetTable LIMIT 10").collect() ]

    print("------Total count by languages Lang, count(*)---")
    [ print(x) for x in sqlContext.sql("SELECT user.lang, COUNT(*) as cnt FROM tweetTable GROUP BY user.lang ORDER BY cnt DESC LIMIT 25").collect() ]

    regexTokenizer = RegexTokenizer(inputCol="text", outputCol="words", pattern="\\W")
    ngram = NGram(n=2, inputCol=regexTokenizer.getOutputCol(), outputCol="ngrams")
    hashingTF = HashingTF(inputCol=regexTokenizer.getOutputCol(), outputCol="features")

    texts = tweetsDF.select("text")
    tokens = regexTokenizer.transform(texts)
    ngrams = ngram.transform(tokens)
    hashes = hashingTF.transform(ngrams)
    
    word2Vec = Word2Vec(vectorSize=1000, minCount=10, seed=10, inputCol="words", outputCol="vector")
    modelW2V = word2Vec.fit(tokens.select("words"))
    wordVectorsDF = modelW2V.getVectors()
    dfW2V = wordVectorsDF.select('vector').withColumnRenamed('vector','features')
    
    # train del modello
    kmeans = KMeans(k=numClusters, seed=1, maxIter=numIterations)
    model = kmeans.fit(dfW2V)
    if os.path.exists(save_model_path + "/tweetmodel"):
        shutil.rmtree(save_model_path + "/tweetmodel")
    # salvataggio del modello
    print("Save model under: %s" % (save_model_path + "/tweetmodel"))
    model.save(save_model_path + "/tweetmodel")
    
    transformed = model.transform(dfW2V).select("features","prediction")
    transformed.groupBy("prediction").agg(count(lit(1))).show()

