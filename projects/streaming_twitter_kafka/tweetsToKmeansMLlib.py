#! /usr/bin/env python3.5

import json
import sys
import os
import shutil
import re
import numpy as np

from pyspark import SparkContext, SQLContext
from pyspark.mllib.feature import Word2Vec
from pyspark.mllib.clustering import KMeans


if __name__ == '__main__': 

    if len(sys.argv) != 5:
        print("Usage: %s  <path to tweets> <save model path> <cluster number> <number of terations>" % (sys.argv[0]))
        sys.exit(1)

    tweets_path = sys.argv[1]
    save_model_path = sys.argv[2]
    numClusters = int(sys.argv[3])
    numIterations = int(sys.argv[4])
    
    assert numClusters > 0
    assert numIterations > 0

    if not os.path.exists(tweets_path):
        print("Non esiste il tweet path")
        sys.exit(1)
    if not os.path.exists(save_model_path):
        print("Non esiste il save model path")
        sys.exit(1)
    
    sc = SparkContext("local[*]", "tweetsToKmeans")
    sc.setLogLevel("WARN")
    sqlContext = SQLContext(sc)
    
    tweets = sc.textFile(tweets_path)
    print("Sample Tweet \n %s" % (tweets.take(1)))

    tweetsDF = sqlContext.read.json(tweets_path).cache()
    tweetsDF.registerTempTable("tweetTable")

    print("------Tweet table Schema---")
    tweetsDF.printSchema()

    print("----Sample Tweet Text-----")
    [ print(x) for x in sqlContext.sql("SELECT text FROM tweetTable LIMIT 10").collect() ]

    print("------Sample Lang, Name, text---")
    [ print(x) for x in sqlContext.sql("SELECT user.lang, user.name, text FROM tweetTable LIMIT 10").collect() ]

    print("------Total count by languages Lang, count(*)---")
    [ print(x) for x in sqlContext.sql("SELECT user.lang, COUNT(*) as cnt FROM tweetTable GROUP BY user.lang ORDER BY cnt DESC LIMIT 25").collect() ]

    texts = sqlContext.sql("SELECT text from tweetTable")

    textsCleaned = texts.rdd \
                   .map(lambda line: line[0].lower() ) \
                   .map(lambda line: re.sub(r'[^\w\s]','',line)) \
                   .map(lambda line: line.split(" ") ) \
                   .map(lambda wordArray: [w for w in wordArray if w != "" and w != "rt"])

    w2v = Word2Vec().setVectorSize(100).setSeed(42).fit(textsCleaned)    
    vectors = sc.parallelize(np.array(list(w2v.getVectors().values()))).cache()

    # train del modello
    model = KMeans.train(vectors, numClusters, numIterations, initializationMode="random")
    if os.path.exists(save_model_path + "/tweetmodel"):
        shutil.rmtree(save_model_path + "/tweetmodel")
    # salvataggio del modello
    print("Save model under: %s" % (save_model_path + "/tweetmodel"))
    model.save(sc, save_model_path + "/tweetmodel")
    some_tweets = vectors.collect()
    keys = list(dict(w2v.getVectors()).keys())
    for n,t in enumerate(some_tweets):
       print(keys[n], model.predict(t))
